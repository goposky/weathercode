#!/usr/local/bin/python3
"""
   Get the weathercode from knmi.nl abc
"""
from http import client
import re
import os

HOST = "www.knmi.nl"
PAGE = "/over-het-knmi/over"

CSS_FORMAT = "weather-small--{}"
CODES = ["green", "yellow", "orange", "red"]


ELEM = '<div class="weather-small">'

PATTERN = re.compile('<div class="weather-small">.*?</div>', flags=re.S)

PREFIX_FORMAT = os.getenv('PREFIX', '')


def handler(request, context):
    """
    use stdlib to connect to `HOST` and check for weathercode based on css.
    """
    print(request, context)
    con = client.HTTPSConnection(HOST)
    con.request("GET", PAGE)
    resp = con.getresponse()
    page = resp.read().decode("utf-8")
    code_match = PATTERN.search(page)
    if code_match:
        code_html = code_match.group()
        for code in CODES:
            if CSS_FORMAT.format(code) in code_html:
                return PREFIX_FORMAT + code
    return None


if __name__ == "__main__":
    print(handler({}, {}))
