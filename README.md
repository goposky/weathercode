# weathercode
The weathercode application retrieves and prints the current weather code as published by the KNMI.
This repository contains a basic gitlab pipeline deploying an application to a target environment. The goal of this demo is to show basic functionality. It is *not* meant as a demonstration of best practices.

## files:

* ./.gitlab-ci.yml: This file contains the gitlab pipelines, this pipeline is triggered by pushing to this repository.
* ./README.md: This is the file you are currently reading
* ./src/weathercode.py: this is python code we are deploying
* ./requirements-dev.txt: this contains the test dependencies of our pipeline

